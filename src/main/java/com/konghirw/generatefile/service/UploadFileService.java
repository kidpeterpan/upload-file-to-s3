package com.konghirw.generatefile.service;

import com.konghirw.generatefile.repository.s3.S3Client;
import org.joda.time.DateTime;

import java.io.File;

public class UploadFileService {

    private S3Client s3Client;

    public void setS3Client(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    public void uploadFile(File file, DateTime targetDate, String bucket) {
        String s3Path = createS3Path(file, targetDate);
        System.out.println("upload to s3://" + bucket + "/" + s3Path);
        s3Client.putFileToS3(bucket, s3Path, file);
    }

    private String createS3Path(File file, DateTime targetDate) {
        return String.format("ext_in/aplus_settlement/%s/%s/%s/%s",targetDate.toString("yyyy"),
                targetDate.toString("MM"),
                targetDate.toString("dd"),
                file.getName());
    }
}
