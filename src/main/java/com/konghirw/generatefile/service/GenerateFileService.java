package com.konghirw.generatefile.service;

import com.konghirw.generatefile.model.GenerateFileRequest;
import com.konghirw.generatefile.util.UUIDGenerator;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GenerateFileService {

    //H|000001|2017-06-06
    private final static String HEADER1 = "H|000001|%s";

    //H|000002|MERCHANT_ID:010000000000872121280|SHOP_ID:0|SETTLEMENT_DATE:2017-06-06|SUM_PAYMENT_AMOUNT:15075|SUM_PAYMENT_FEE_INCLUDED_AMOUNT:15075|SUM_REFUND_AMOUNT:15075|SUM_REFUND_FEE_INCLUDED_AMOUNT:15075|SUM_SETTLED_AMOUNT:0|LAST_UNSETTLED_AMOUNT:0
    private final static String HEADER2 = "H|000002|MERCHANT_ID:%s|SHOP_ID:%s|SETTLEMENT_DATE:%s|SUM_PAYMENT_AMOUNT:%s|SUM_PAYMENT_FEE_INCLUDED_AMOUNT:%s|SUM_REFUND_AMOUNT:0|SUM_REFUND_FEE_INCLUDED_AMOUNT:0|SUM_SETTLED_AMOUNT:%s|LAST_UNSETTLED_AMOUNT:0|SETTLEMENT_LEVEL:%s|PROJECT_CODE:ONLINEPAYMENT";

    //H|000003|PARTNER_TRANSACTION_ID|TRANSACTION_ID|AMOUNT|FEE|VAT|FEE_INCLUDED|SETTLEMENT|CURRENCY|WHT|PAYMENT_TIME|SETTLEMENT_TIME|TYPE|SOF|REMARK
    private final static String HEADER3 = "H|000003|PARTNER_TRANSACTION_ID|PARTNER_MERCHANT_ID|PARTNER_SHOP_ID|PARTNER_SHOP_NAME|PARTNER_TERMINAL_ID|TRANSACTION_ID|AMOUNT|FEE|VAT|FEE_INCLUDED|SETTLEMENT|CURRENCY|WHT|PAYMENT_TIME|SETTLEMENT_TIME|TYPE|SOF|MERCHANT_ID|MERCHANT_NAME|SHOP_ID|SHOP_NAME|TERMINAL_ID|REMARK";

    //D|000004|201603021110166219501170243|TC_POP_00111_1|15075|425|30|13|14620|THB|0|2016-01-04T00:00:00+07:00|2016-06-06|P|TMN|
    private final static String DATA = "D|%06d|%s|MOCK_PARTNER_MERCHANT_ID|MOCK_PARTNER_SHOP_ID|MOCK_PARTNER_SHOP_NAME|PARTNER_TERMINAL_ID|MOCK_LOADTEST|10000|1000|100|1100|8900|THB|0|%s|%s|P|CARD|%s|MOCK_MERCHANT_NAME|%s|MOCK_SHOP_NAME|90000|remark";

    //T|000003
    private final static String TAIL = "T|%06d";

    public File generateAplusFile(GenerateFileRequest request) {
        String filePath = createFileName(request.getMerchantId(), request.getContractId(), request.getTargetDate(), request.getShopId(), request.getSettleLevel());
        File writedFile = new File(filePath);
        DateTime originPaymentDate = request.getTargetDate().minusDays(1);
        DateTime originSettleDate = request.getTargetDate();
        int totalTrans = request.getNumOfDays() * request.getNumOfTrans();
        int totalAmount = totalTrans * 10000;
        int totalFeeInclude = totalTrans * 1100;
        int totalSettle = totalTrans * 8900;
        String headerShopId = "";
        if("SHOP".equals(request.getSettleLevel())) {
            headerShopId = request.getShopId();
        }else{
            headerShopId = "0";
        }
        try (BufferedWriter writer = openFile(writedFile)) {
            writer.write(String.format(HEADER1,
                    originSettleDate.toString("yyyy-MM-dd")));
            writer.newLine();
            writer.append(String.format(HEADER2,
                    request.getMerchantId(),
                    headerShopId,
                    originSettleDate.toString("yyyy-MM-dd"),
                    totalAmount,
                    totalFeeInclude,
                    totalSettle,
                    request.getSettleLevel()));
            writer.newLine();
            writer.append(HEADER3);
            writer.newLine();
            int writedLine = 4;
            for (int i =0; i < request.getNumOfDays(); i ++) {
                DateTime paymentDate = originPaymentDate.minusDays(i);
                for( int j=0; j < request.getNumOfTrans(); j++) {
                    writer.append(String.format(DATA,
                            writedLine++,
                            UUIDGenerator.generate(),
                            paymentDate.toString(ISODateTimeFormat.dateTimeNoMillis()),
                            originSettleDate.toString("yyyy-MM-dd"),
                            request.getMerchantLevelId(),
                            request.getShopId()
                    ));
                    writer.newLine();
                }
            }
            writer.append(String.format(TAIL, totalTrans));
            return writedFile;
        } catch (IOException ex) {
            throw new RuntimeException("Could not write file", ex);
        }

    }

    private String createFileName(String merchantId, String contractId , DateTime targetDate, String shopId,String settleLevel) {
        if("SHOP".equals(settleLevel))
            return String.format("%s_%s_%s_settlement_%s.txt", merchantId,shopId, contractId, targetDate.toString("yyyyMMdd"));
        else
            return String.format("%s_0_%s_settlement_%s.txt", merchantId, contractId, targetDate.toString("yyyyMMdd"));
    }

    private BufferedWriter openFile(File file) throws IOException{
        return new BufferedWriter((new FileWriter(file)));
    }
}
