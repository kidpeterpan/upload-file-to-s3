package com.konghirw.generatefile;

import com.konghirw.generatefile.job.GenerateJobBuilder;
import com.konghirw.generatefile.model.AppInputParameter;
import org.joda.time.DateTime;

public class MainApp {

    public static void main(String[] argv) {

        if (argv.length == 0) {
            argv = new String[1];
            argv[0] = "0";
        }

        if("0".equals(argv[0])) {
            String bucketName = "awsa-ttmn-operation-file";
            String merchatFilePath = "/Users/kidpeterpan/generatefile/example-files/merchant_profile_shop.csv";
            String numOfDays = "5";
            String numOfTrans = "10";
            String targetDate = "2019-06-01";
            AppInputParameter appInputParameter = new AppInputParameter();
            appInputParameter.setBucket(bucketName); // awst-ttmn-operation-file
            appInputParameter.setMerchatFilePath(merchatFilePath); // data/merchant_profile_corperate.csv
            appInputParameter.setNumOfDays(Integer.valueOf(numOfDays)); // 1
            appInputParameter.setNumOfTrans(Integer.valueOf(numOfTrans)); // 1
            appInputParameter.setTargetDate(new DateTime(targetDate)); // 2018-07-09
            GenerateJobBuilder.createDefaultJob().generate(appInputParameter);
        }else if("1".equals(argv[0])){
            AppInputParameter appInputParameter = new AppInputParameter();
            appInputParameter.setBucket(argv[1]); // awst-ttmn-operation-file
            appInputParameter.setMerchatFilePath(argv[2]); // data/merchant_profile_corperate.csv
            appInputParameter.setNumOfDays(Integer.valueOf(argv[3])); // 1
            appInputParameter.setNumOfTrans(Integer.valueOf(argv[4])); // 1
            appInputParameter.setTargetDate(new DateTime(argv[5])); // 2018-07-09
            GenerateJobBuilder.createDefaultJob().generate(appInputParameter);
        }
    }

}
