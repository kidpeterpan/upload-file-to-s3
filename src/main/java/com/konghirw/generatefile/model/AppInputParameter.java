package com.konghirw.generatefile.model;

import org.joda.time.DateTime;

public class AppInputParameter {

    private String path;

    private String bucket;

    private DateTime targetDate;

    private int numOfDays;

    private int numOfTrans;

    private String merchatFilePath;

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public DateTime getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(DateTime targetDate) {
        this.targetDate = targetDate;
    }

    public int getNumOfDays() {
        return numOfDays;
    }

    public void setNumOfDays(int numOfDays) {
        this.numOfDays = numOfDays;
    }

    public int getNumOfTrans() {
        return numOfTrans;
    }

    public void setNumOfTrans(int numOfTrans) {
        this.numOfTrans = numOfTrans;
    }

    public String getMerchatFilePath() {
        return merchatFilePath;
    }

    public void setMerchatFilePath(String merchatFilePath) {
        this.merchatFilePath = merchatFilePath;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
