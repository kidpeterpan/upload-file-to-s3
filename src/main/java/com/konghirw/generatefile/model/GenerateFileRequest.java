package com.konghirw.generatefile.model;

import org.joda.time.DateTime;

public class GenerateFileRequest {

    private DateTime targetDate;

    private int numOfDays;

    private int numOfTrans;

    private String merchantId;

    private String contractId;

    private String settleLevel;

    private String shopId;

    private String merchantLevelId;

    public DateTime getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(DateTime targetDate) {
        this.targetDate = targetDate;
    }

    public int getNumOfDays() {
        return numOfDays;
    }

    public void setNumOfDays(int numOfDays) {
        this.numOfDays = numOfDays;
    }

    public int getNumOfTrans() {
        return numOfTrans;
    }

    public void setNumOfTrans(int numOfTrans) {
        this.numOfTrans = numOfTrans;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getSettleLevel() {
        return settleLevel;
    }

    public void setSettleLevel(String settleLevel) {
        this.settleLevel = settleLevel;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getMerchantLevelId() {
        return merchantLevelId;
    }

    public void setMerchantLevelId(String merchantLevelId) {
        this.merchantLevelId = merchantLevelId;
    }
}
