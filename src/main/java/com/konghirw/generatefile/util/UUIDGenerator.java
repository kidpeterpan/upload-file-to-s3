package com.konghirw.generatefile.util;

import org.joda.time.DateTime;

import java.util.UUID;

public class UUIDGenerator {
    public static String generate() {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replace("-", "");
		return uuid;
	}

	public static String generatePK() {
		String uuid = UUID.randomUUID().toString();
		uuid = DateTime.now().getYear() + "-" + uuid;
		return uuid;
	}
}
