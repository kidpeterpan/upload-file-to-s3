package com.konghirw.generatefile.job;


import com.konghirw.generatefile.repository.s3.S3Client;
import com.konghirw.generatefile.service.GenerateFileService;
import com.konghirw.generatefile.service.UploadFileService;

public class GenerateJobBuilder {

    public static GenerateJob createDefaultJob() {
        S3Client s3Client = new S3Client();
        s3Client.init();
        GenerateFileService service = new GenerateFileService();
        UploadFileService uploadFileService = new UploadFileService();
        uploadFileService.setS3Client(s3Client);
        GenerateJob job = new GenerateJob();
        job.setGenerateFileService(service);
        job.setUploadFileService(uploadFileService);
        return job;
    }

}
