package com.konghirw.generatefile.job;

import com.konghirw.generatefile.model.AppInputParameter;
import com.konghirw.generatefile.model.GenerateFileRequest;
import com.konghirw.generatefile.service.GenerateFileService;
import com.konghirw.generatefile.service.UploadFileService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class GenerateJob {


    private GenerateFileService generateFileService;

    private UploadFileService uploadFileService;

    public void setGenerateFileService(GenerateFileService generateFileService) {
        this.generateFileService = generateFileService;
    }

    public void setUploadFileService(UploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    public void generate(AppInputParameter input) {
        String merchantFilePath = input.getMerchatFilePath();
        try (Stream<String> lines = Files.lines(Paths.get(merchantFilePath))) {
            lines.forEach( eachline -> {
                if(StringUtils.isBlank(eachline)) {
                    return;
                }
                String[] eachPath = eachline.split(",");
                GenerateFileRequest request = new GenerateFileRequest();
                request.setMerchantId(eachPath[0]);
                request.setContractId(eachPath[1]);
                request.setShopId(eachPath[2]);
                request.setMerchantLevelId(eachPath[3]);
                request.setTargetDate(input.getTargetDate());
                request.setNumOfTrans(input.getNumOfTrans());
                request.setNumOfDays(input.getNumOfDays());
                String[] arrStr = input.getMerchatFilePath().split("_");
                String settleLevel = arrStr[arrStr.length-1].replace(".csv", "");
                request.setSettleLevel(settleLevel.toUpperCase());
                if("SHOP".equals(request.getSettleLevel())){
                    request.setMerchantLevelId(request.getMerchantId());
                }

                File generatedFile = generateFileService.generateAplusFile(request);
                uploadFileService.uploadFile(generatedFile, input.getTargetDate(), input.getBucket());
                FileUtils.deleteQuietly(generatedFile);
            });
        } catch (IOException ex) {
            throw new RuntimeException("Could not read file", ex);
        }
    }

}
