package com.konghirw.generatefile.repository.s3;


import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.konghirw.generatefile.repository.s3.factory.AmazonS3Factory;

import java.io.File;

public class S3Client {

    private AmazonS3 s3;

    public void init() {
        s3 = AmazonS3Factory.createDefaultClient();
    }

    public void putFileToS3(String bucketName, String key, File file) {
        s3.putObject(bucketName, key, file);
    }

    public S3Object getObject(String bucket, String key) {
        return s3.getObject(new GetObjectRequest(bucket, key));
    }

    public void moveFileToTargetPath(String bucketName, String sourcepath, String descpath) {
        s3.copyObject(new CopyObjectRequest(bucketName, sourcepath, bucketName, descpath));
        s3.copyObject(bucketName, sourcepath, bucketName, descpath);
        s3.deleteObject(bucketName, sourcepath);
    }

    public void deleteObject(String bucketName, String key) {
        s3.deleteObject(bucketName, key);
    }

}
