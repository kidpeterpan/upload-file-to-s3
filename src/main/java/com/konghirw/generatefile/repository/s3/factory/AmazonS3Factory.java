package com.konghirw.generatefile.repository.s3.factory;


import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public class AmazonS3Factory {

    public static AmazonS3 createDefaultClient() {
        return AmazonS3ClientBuilder.defaultClient();
    }

}
