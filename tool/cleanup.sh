#!/bin/bash

# remove all file(s) in floder
aws s3 rm --recursive s3://awsa-ttmn-operation-file/ext_in/aplus_settlement/2019/06/01/
# list file(s) in directory
aws s3 ls s3://awsa-ttmn-operation-file/ext_in/aplus_settlement/2019/06/01/
# remove per file
aws s3 rm s3://awsa-ttmn-operation-file/ext_in/aplus_settlement/2019/08/06/010000000003004202891_0_200000000000000216730_settlement_20190806.txt

